// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

// TODO: Most of these header could go into the cpp file.
#include "Editor/PropertyEditor/Public/IPropertyTypeCustomization.h"
#include "Editor/PropertyEditor/Public/IPropertyUtilities.h"
#include "Utils/InterfaceClass.h"
#include "SWidget.h"

/**
 * Property customization for InterfaceClass
 */
class GAMEREADYAIEDITOR_API FInterfaceClassCustomization : public IPropertyTypeCustomization
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance();

	virtual void CustomizeHeader(TSharedRef<class IPropertyHandle> StructPropertyHandle, class FDetailWidgetRow& HeaderRow,
		IPropertyTypeCustomizationUtils& StructCustomizationUtils) override;

	virtual void CustomizeChildren(TSharedRef<IPropertyHandle> PropertyHandle, IDetailChildrenBuilder& ChildBuilder,
		IPropertyTypeCustomizationUtils& CustomizationUtils) override;

private:
	TSharedPtr<IPropertyHandle> ClassHandle;
	FInterfaceClass* InterfaceClass;

	bool IsBlueprintInterface(UClass* Class) const;
	TSharedRef<SWidget> OnGetInterfaceListWidget() const;

	void OnSelection(UClass* Class);
	FText GetClassName() const;

	FInterfaceClass* EnsureInterfaceClass() const;
	bool IsInterfaceClassDefined() const;
};
