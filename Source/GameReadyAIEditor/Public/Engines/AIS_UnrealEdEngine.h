// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Editor/UnrealEdEngine.h"
#include "AIS_DataManagerProvider.h"
#include "AIS_UnrealEdEngine.generated.h"

/**
 * A custom UnrealEd engine to manage persistent data.
 */
UCLASS()
class GAMEREADYAIEDITOR_API UAIS_UnrealEdEngine : public UUnrealEdEngine, public IAIS_DataManagerProvider
{
	GENERATED_BODY()

public:

	UAIS_UnrealEdEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	class UAIS_DataManager* GetDataManager() override { return DataManager; };

protected:

	UPROPERTY()
	class UAIS_DataManager* DataManager;
};
