// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Editor/EditorEngine.h"
#include "AIS_DataManagerProvider.h"
#include "AIS_EditorEngine.generated.h"

/**
 * A custom EditorEngine to manage persistent data. (Not sure whether this is needed for data management, but
 * a custom editor started with the default UEditorEngine as EditorEngine crashes when initializing UObject.)
 */
UCLASS()
class GAMEREADYAIEDITOR_API UAIS_EditorEngine : public UEditorEngine, public IAIS_DataManagerProvider
{
	GENERATED_BODY()

public:
	UAIS_EditorEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	class UAIS_DataManager* GetDataManager() override { return DataManager; };

protected:

	UPROPERTY()
	class UAIS_DataManager* DataManager;
};
