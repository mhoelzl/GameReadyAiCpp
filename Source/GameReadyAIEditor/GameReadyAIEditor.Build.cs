// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GameReadyAIEditor : ModuleRules
{
	public GameReadyAIEditor(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine",
            "GameReadyAI" });
        PrivateDependencyModuleNames.AddRange(new string[] { "InputCore", "UnrealEd",
            "SlateCore", "Slate" });
        PublicIncludePaths.AddRange(new string[] { "GameReadAIEditor/Public" });
        PrivateIncludePaths.AddRange(new string[] { "GameReadyAIEditor/Private" });
	}
}
