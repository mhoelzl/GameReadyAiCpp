#include "GameReadyAIEditor.h"
#include "Editor/PropertyEditor/Public/PropertyEditorModule.h"
#include "InterfaceClassCustomization.h"


IMPLEMENT_GAME_MODULE(FGameReadyAIEditorModule, GameReadyAIEditor);

DEFINE_LOG_CATEGORY(AIS_Editor)

#define LOCTEXT_NAMESPACE "GameReadyAIEditor"

void FGameReadyAIEditorModule::StartupModule()
{
	UE_LOG(AIS_Editor, Log, TEXT("GameReadyAIEditor: StartupModule()"));
	FPropertyEditorModule& PropertyEditorModule{ 
		FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor") };

	PropertyEditorModule.RegisterCustomPropertyTypeLayout(FName(TEXT("InterfaceClass")),
		FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FInterfaceClassCustomization::MakeInstance));
}

void FGameReadyAIEditorModule::ShutdownModule()
{
	UE_LOG(AIS_Editor, Log, TEXT("GameReadyAIEditor: ShutdownModule()"));
}

#undef LOCTEXT_NAMESPACE
