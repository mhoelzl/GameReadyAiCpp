// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAIEditor.h"
#include "AIS_DataManager.h"
#include "AIS_UnrealEdEngine.h"


UAIS_UnrealEdEngine::UAIS_UnrealEdEngine(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	DataManager = CreateDefaultSubobject<UAIS_DataManager>(TEXT("DataManager"));
	check(DataManager != nullptr && DataManager->IsValidLowLevel());
}
