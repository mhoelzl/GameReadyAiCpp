// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "GameFramework/Character.h"
#include "GenericTeamAgentInterface.h"
#include "Interfaces/ItemUser.h"
#include "Runtime/UMG/Public/Components/WidgetComponent.h"
#include "Stats/Stat.h"
#include "AIS_Character.generated.h"

UCLASS(Blueprintable, BlueprintType)
class GAMEREADYAI_API AAIS_Character :
	public ACharacter, public IItemUser, 
	// We don't currently make use of this interface...
	public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAIS_Character(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void PostLoad() override;
	virtual void PostActorCreated() override;
	
	virtual void PostInitializeComponents() override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// The widget for the stat display over the character's head
	UPROPERTY(EditAnywhere, Category = "UMG")
	UWidgetComponent* StatWidget;

	// The rate with which the stamina loss of the character is ticked.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float StaminaTickTime; // Called StamTickRate in the video

		// The we apply to stamina for each tick.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float StaminaDecayRate;

	// The rate with which the character heals itself
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float HealRate;

	// The health of the character loses when its stamina is 0; must be at least 0.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float HealthLossOnZeroStamina;

	FTimerHandle StaminaTickTimer;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator,
		AActor* DamageCauser) override;

	// IItemUser interface
	virtual float EstimateUtilityOf(const AActor* ItemToUse) const override;

	virtual bool UseItemInWorld(AActor* ItemToUse) override;

	virtual float AddToCurrentStatValue(FName StatName, float Delta) override;

	virtual TArray<UStat*> GetAllStats(void) const override;
	// End IItemUser interface (see below for AddToCurrentStatValue)

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
	float UseRange;

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	UStat* GetStat(FName StatName) { return StatMap[StatName]; };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	UStat* GetStatOrNullptr(FName StatName) { return StatMap.FindRef(StatName); };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	UStat* GetStatByIndex(int32 Index) { return Stats[Index]; };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	float GetCurrentStatValue(FName StatName) { return GetStat(StatName)->GetCurrentValue(); };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	float GetCurrentStatPercentage(FName StatName) { return GetStat(StatName)->GetCurrentPercentage(); };

	UFUNCTION(BlueprintCallable, Category = "Player Stats")
	void SetCurrentStatValue(FName StatName, float NewValue) { GetStat(StatName)->SetCurrentValue(NewValue); };

	// AddToCurrentStatValue inherited from IItemUser (see BPAddToCurrentStatValue_Implementation);

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	float GetMinStatValue(FName StatName) { return GetStat(StatName)->GetMinValue(); };

	UFUNCTION(BlueprintCallable, Category = "Player Stats")
	void SetMinStatValue(FName StatName, float NewValue) { GetStat(StatName)->SetMinValue(NewValue); };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	float GetMaxStatValue(FName StatName) { return GetStat(StatName)->GetMaxValue(); };

	UFUNCTION(BlueprintCallable, Category = "Player Stats")
	void SetMaxStatValue(FName StatName, float NewValue) { GetStat(StatName)->SetMaxValue(NewValue); };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	float GetStatImportanceFactor(FName StatName) { return GetStat(StatName)->GetImportanceFactor(); };

	UFUNCTION(BlueprintCallable, Category = "Player Stats")
	void SetStatImportanceFactor(FName StatName, float NewValue) { GetStat(StatName)->SetImportanceFactor(NewValue); };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	TArray<UStat*> GetStats() { return Stats; };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	bool HasStat(FName StatName) { return StatMap.Contains(StatName); };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	bool AreStatsInitialized() { return bAreStatsInitialized; };

protected:
	// InitializeStats makes sure that Stats and StatMap contain the same elements. Users might easily
	// subvert this by modifying the Stats array returned from GetStats(). Maybe we should keep only 
	// StatMap and generate the Stats array on demand?
	
	// I'm not sure that the Instanced keyword does the right thing here. When I leave it out the individual
	// stats are no longer editable in the editor. On the other hand it sounds as if this might create copies
	// of the elements stored in this array which is most definitely not what we want. --tc
	// 
	UPROPERTY(EditAnywhere, Instanced, Category = "Player Stats", Meta = (AllowPrivateAccess = "true"))
	TArray<UStat*> Stats;

	UPROPERTY()
	TMap<FName, UStat*> StatMap;

	UPROPERTY(EditAnywhere, Category = "Player Stats")
	bool bAreStatsInitialized;

	void InitializeSkeletalMesh();
	void InitializeStatWidget();
	void InitializeMovementComponent();
	void InitializeAI();
	void InitializeStats();

	void TickDownStamina();
	bool CanReachItem(AActor* ItemToUse);

};

