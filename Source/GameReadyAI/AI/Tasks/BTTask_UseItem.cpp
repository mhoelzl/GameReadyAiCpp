// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AIController.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BlackboardData.h"
#include "BTTask_UseItem.h"
#include "Interfaces/IsUsable.h"
#include "Interfaces/ItemUser.h"


UBTTask_UseItem::UBTTask_UseItem(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	NodeName = TEXT("Use Item");
	auto DesiredObjectKeyProperty = GET_MEMBER_NAME_CHECKED(UBTTask_UseItem, DesiredObjectKey);
	DesiredObjectKey.AddObjectFilter(this, DesiredObjectKeyProperty, AActor::StaticClass());
}

void UBTTask_UseItem::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);

	UBlackboardData* BlackboardAsset{ GetBlackboardAsset() };
	if (ensure(BlackboardAsset))
	{
		DesiredObjectKey.ResolveSelectedKey(*BlackboardAsset);
	}
}

EBTNodeResult::Type UBTTask_UseItem::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	IItemUser* User{ Cast<IItemUser>(OwnerComp.GetAIOwner()->GetPawn()) };
	if (User == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	UBlackboardComponent* BlackboardComp{ OwnerComp.GetBlackboardComponent() };
	if (ensure(BlackboardComp))
	{
		AActor* Item{ Cast<AActor>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(DesiredObjectKey.GetSelectedKeyID())) };
		
		if (User && Item && !Item->IsPendingKill() && Item->IsValidLowLevel())
		{
			if (User->UseItemInWorld(Item)) {
				return EBTNodeResult::Succeeded;
			}
		}
	}
	return EBTNodeResult::Failed;
}

FString UBTTask_UseItem::GetStaticDescription() const
{
	return FString::Printf(TEXT("Use %s"), *DesiredObjectKey.SelectedKeyName.ToString());
}
