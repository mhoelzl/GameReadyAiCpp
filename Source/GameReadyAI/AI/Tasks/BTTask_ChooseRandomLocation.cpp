// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BTTask_ChooseRandomLocation.h"

UBTTask_ChooseRandomLocation::UBTTask_ChooseRandomLocation(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	MaxRadius{ 3000.0f }
{
	NodeName = TEXT("Choose Random Location");
	MoveToKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_ChooseRandomLocation, MoveToKey));
}

void UBTTask_ChooseRandomLocation::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);

	UBlackboardData* BlackboardAsset{ GetBlackboardAsset() };
	if (ensure(BlackboardAsset))
	{
		MoveToKey.ResolveSelectedKey(*BlackboardAsset);
	}
}

EBTNodeResult::Type UBTTask_ChooseRandomLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (ensure(MoveToKey.IsSet()))
	{
		FVector MyLocation{ OwnerComp.GetAIOwner()->GetPawn()->GetActorLocation() };

		FNavLocation NewNavigation;
		UNavigationSystem::GetNavigationSystem(this)->GetRandomPointInNavigableRadius(MyLocation, MaxRadius, NewNavigation);

		UBlackboardComponent* BlackboardComp{ OwnerComp.GetBlackboardComponent() };
		if (ensure(BlackboardComp))
		{
			BlackboardComp->SetValue<UBlackboardKeyType_Vector>(MoveToKey.GetSelectedKeyID(), NewNavigation.Location);
		}

		return EBTNodeResult::Succeeded;
	}
	else
	{
		return EBTNodeResult::Failed;
	}
}

FString UBTTask_ChooseRandomLocation::GetStaticDescription() const
{
	return FString::Printf(TEXT("Choose a random location within a %.1fm radius"), MaxRadius / 100.0f);
}
