// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Perception/AISense.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISense_Sight.h"
#include "AIS_AIController.h"

AAIS_AIController::AAIS_AIController(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	PerceptionRetentionTime{ 3.0f }
{
	static auto BTFinder{ ConstructorHelpers::FObjectFinder<UBehaviorTree>(TEXT("BehaviorTree'/Game/AIStream/AI/AIS_BT.AIS_BT'")) };
	if (BTFinder.Succeeded())
	{
		BehaviorTree = BTFinder.Object;
	}
	InitializePerceptionComponent();
}

void AAIS_AIController::BeginPlay()
{
	Super::BeginPlay();
	check(BehaviorTree && "No behavior tree for AIS AIController.");
	RunBehaviorTree(BehaviorTree);
}

void AAIS_AIController::InitializePerceptionComponent()
{
	UAIPerceptionComponent* MyPerceptionComponent{
		CreateDefaultSubobject<UAIPerceptionComponent>("PerceptionComponent") };
	SetPerceptionComponent(*MyPerceptionComponent);

	check(MyPerceptionComponent != nullptr);
	// PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));
	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfig"));

	SightConfig->SightRadius = 3000.0f;
	SightConfig->LoseSightRadius = 3500.0f;
	SightConfig->PeripheralVisionAngleDegrees = 90.0f;
	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	// This is a cruel, cruel joke you're playing on me, right?
	// Please tell me I'm missing something and there is a function hidden
	// somewhere in the code that does all this in a sensible manner...
	UFloatProperty* MaxAgeProperty{
		Cast<UFloatProperty>(UAISenseConfig::StaticClass()->FindPropertyByName(FName(TEXT("MaxAge")))) };
	if (ensure(MaxAgeProperty))
	{
		uint8* ConfigPtr{ MaxAgeProperty->ContainerPtrToValuePtr<uint8>(SightConfig) };
		MaxAgeProperty->SetFloatingPointPropertyValue(ConfigPtr, PerceptionRetentionTime);
	}

	MyPerceptionComponent->ConfigureSense(*SightConfig);
	MyPerceptionComponent->SetDominantSense(SightConfig->GetSenseImplementation());
	MyPerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AAIS_AIController::UpdatePerception);
	MyPerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &AAIS_AIController::UpdateTargetPerception);
}


void AAIS_AIController::UpdatePerception(TArray<AActor*> UpdatedActors)
{
	// GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, 
	// 	FString::Printf(TEXT("I see many Actors: %d"), UpdatedActors.Num()));
}

void AAIS_AIController::UpdateTargetPerception(AActor* Actor, FAIStimulus Stimulus)
{
	// UE_LOG(LogTemp, Log, TEXT("Perceiving actor: %s"), *GetNameSafe(Actor));
}