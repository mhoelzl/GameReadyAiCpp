// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "BehaviorTree/BTService.h"
#include "BTService_StaminaCheck.generated.h"

/**
* A service that checks whether the character should go and find food.
*/
UCLASS()
class GAMEREADYAI_API UBTService_StaminaCheck : public UBTService
{
	GENERATED_BODY()
	
public:
	UBTService_StaminaCheck(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	struct FBlackboardKeySelector FindFoodKey;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float StaminaLowLevel;
};
