// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AIController.h"
#include "AI/AIS_Character.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Stats/StatNames.h"
#include "BTService_StaminaCheck.h"

UBTService_StaminaCheck::UBTService_StaminaCheck(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer),
	StaminaLowLevel{ 50.0f }
{

}

void UBTService_StaminaCheck::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);

	UBlackboardData* BBAsset{ GetBlackboardAsset() };
	if (ensure(BBAsset))
	{
		FindFoodKey.ResolveSelectedKey(*BBAsset);
	}
}

void UBTService_StaminaCheck::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AAIS_Character* Character{ Cast<AAIS_Character>(OwnerComp.GetAIOwner()->GetPawn()) };
	UBlackboardComponent* BlackboardComp{ OwnerComp.GetBlackboardComponent() };

	if (ensure(Character))
	{
		bool FindFoodValue{ Character->GetCurrentStatValue(StatName::Stamina) <= StaminaLowLevel };
		BlackboardComp->SetValueAsBool(FindFoodKey.SelectedKeyName, FindFoodValue);
	}
}
