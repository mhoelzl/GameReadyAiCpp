// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "AIController.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AIPerceptionComponent.h"
#include "WidgetComponent.h"
#include "AIS_AIController.generated.h"

UCLASS()
class GAMEREADYAI_API AAIS_AIController : public AAIController
{
	GENERATED_BODY()

public:
	AAIS_AIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AI")
	class UBehaviorTree* BehaviorTree;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Perception")
	UAISenseConfig_Sight* SightConfig;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Perception")
	float PerceptionRetentionTime;

	UFUNCTION()
	void UpdatePerception(TArray<AActor*> UpdatedActors);

	UFUNCTION()
	void UpdateTargetPerception(AActor* Actor, FAIStimulus Stimulus);

protected:
	void InitializePerceptionComponent();
};
