// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "EnvironmentQuery/EnvQueryGenerator.h"
#include "EQSGenerator_PerceivedActors.generated.h"

/**
 * A generator for all perceived actors.
 */
UCLASS()
class GAMEREADYAI_API UEQSGenerator_PerceivedActors : public UEnvQueryGenerator
{
	GENERATED_BODY()
	
public:
	UEQSGenerator_PerceivedActors(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	
	// The base class of actors we are interested in
	UPROPERTY(EditDefaultsOnly, Category = "Generator")
	TSubclassOf<AActor> ActorBaseClass;
	
	// The context for selecting the center of the query
	UPROPERTY(EditDefaultsOnly, Category = "Generator")
	TSubclassOf<UEnvQueryContext> QueryContext;
	
	virtual void GenerateItems(FEnvQueryInstance& QueryInstance) const override;

	virtual FText GetDescriptionTitle() const override;
	virtual FText GetDescriptionDetails() const override;

protected:
	bool IsWorldWithPerception(const UWorld* World) const;
};
