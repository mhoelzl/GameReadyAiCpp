// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "EnvironmentQuery/EnvQueryGenerator.h"
#include "DataProviders/AIDataProvider.h"
#include "Utils/InterfaceClass.h"
#include "EQSGenerator_ActorsWithInterface.generated.h"

/**
 * A generator for all actors that implement a given interface.
 */
UCLASS(Meta = (DisplayName = "Actors with Interface"))
class GAMEREADYAI_API UEQSGenerator_ActorsWithInterface : public UEnvQueryGenerator
{
	GENERATED_BODY()

public:

	UEQSGenerator_ActorsWithInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	
	// Max Radius of the circle around the context we are searching
	UPROPERTY(EditDefaultsOnly, Category = "Generator")
	FAIDataProviderFloatValue SearchRadius;

	// The base class of the actors we are interested in. 
	UPROPERTY(EditDefaultsOnly, Category = "Generator")
	TSubclassOf<AActor> ActorBaseClass;

	// The interface we are searching for.
	UPROPERTY(EditDefaultsOnly, Category = "Generator")
	FInterfaceClass Interface;

	// The context for selecting the center of the query.
	UPROPERTY(EditAnywhere, Category = Generator)
	TSubclassOf<UEnvQueryContext> SearchCenter;

	virtual void GenerateItems(FEnvQueryInstance& QueryInstance) const override;

	virtual FText GetDescriptionTitle() const override;
	virtual FText GetDescriptionDetails() const override;

};
