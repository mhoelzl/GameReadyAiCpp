// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AI/AIS_AIController.h"
#include "AI/AIS_Character.h"
#include "EnvironmentQuery/Contexts/EnvQueryContext_Querier.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EQSGenerator_PerceivedActors.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AIPerceptionComponent.h"

#define LOCTEXT_NAMESPACE "GameReadyAI"


UEQSGenerator_PerceivedActors::UEQSGenerator_PerceivedActors(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	QueryContext = UEnvQueryContext_Querier::StaticClass();
	ItemType = UEnvQueryItemType_Actor::StaticClass();
	ActorBaseClass = AActor::StaticClass();
}

void UEQSGenerator_PerceivedActors::GenerateItems(FEnvQueryInstance& QueryInstance) const
{
	if (ActorBaseClass == nullptr)
	{
		return;
	}

	APawn* QueryOwner{ Cast<APawn>(QueryInstance.Owner.Get()) };
	if (QueryOwner == nullptr)
	{
		return;
	}

	UWorld* World{ GEngine->GetWorldFromContextObject(QueryOwner) };
	if (World == nullptr)
	{
		return;
	}

	// CHECK: Do we need to do this? We use neither the context locations
	// nor the query context in the following, but maybe PrepareContext() initializes
	// something else in EQS? Look at the code to figure this out. --tc
	TArray<FVector> ContextLocations;
	QueryInstance.PrepareContext(QueryContext, ContextLocations);

	TArray<AActor*> PerceivedActors;
	AAIS_AIController* Controller{ Cast<AAIS_AIController>(QueryOwner->GetController()) };

	UAIPerceptionComponent* PerceptionComponent{
		Cast<UAIPerceptionComponent>((Controller != nullptr) ?
			Controller->GetPerceptionComponent() :
			QueryOwner->FindComponentByClass(UAIPerceptionComponent::StaticClass())) };

#if WITH_EDITOR
	// This first code path is purely for the EQS testing pawn: The pawn should display
	// its evaluation in the editor, when we don't necessarily have a working perception
	// system. 
	if (PerceptionComponent == nullptr || IsWorldWithPerception(World) == false)
	{
		if (PerceptionComponent)
		{
			UE_LOG(LogTemp, Warning, TEXT("EQS \"perceived actors\" query without perception system. Using all actors."));
			UE_LOG(LogTemp, Warning, TEXT("QueryType: %d"), static_cast<int32>(World->WorldType));
		}
		for (TActorIterator<AActor> It(World, ActorBaseClass); It; ++It)
		{
			PerceivedActors.Add(*It);
		}
	}
	// The default path that should always be used in the game.
	else
	{
		PerceptionComponent->GetPerceivedActors(UAISense_Sight::StaticClass(), PerceivedActors);
	}
#else
	if (PerceptionComponent != nullptr)
	{
		PerceptionComponent->GetPerceivedActors(UAISense_Sight::StaticClass(), PerceivedActors);
	}
#endif

	for (AActor* Actor : PerceivedActors)
	{
		if (Actor != nullptr && Actor->IsA(ActorBaseClass) && !Actor->IsPendingKill())
		{
			QueryInstance.AddItemData<UEnvQueryItemType_Actor>(Actor);
		}
	}
}

FText UEQSGenerator_PerceivedActors::GetDescriptionTitle() const
{
	return Super::GetDescriptionTitle();
}

FText UEQSGenerator_PerceivedActors::GetDescriptionDetails() const
{
	return Super::GetDescriptionDetails();
}

// This test is not 100% accurate, but it should be good enough.
bool UEQSGenerator_PerceivedActors::IsWorldWithPerception(const UWorld* World) const
{
	EWorldType::Type WorldType{ World->WorldType };
	// CHECK: I think that some streaming levels may have EWorldType::None in game, so
	// that EWorldType::Game, EWorldType::Running and EWorldType::PIE are the cases 
	// that may happen while the game is running. Not sure how to figure out, whether
	// this is true, though. --tc
	return WorldType == EWorldType::Game
		|| WorldType == EWorldType::None
		|| WorldType == EWorldType::PIE;
}

#undef LOCTEXT_NAMESPACE