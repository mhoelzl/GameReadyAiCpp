// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Blueprint/UserWidget.h"
#include "AI/AIS_Character.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Stats/Stat.h"
#include "StatBlockBase.generated.h"

/**
 * A widget that displays multiple StatBars
 */
UCLASS()
class GAMEREADYAI_API UStatBlockBase : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UStatBlockBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetOwnerCharacter(AAIS_Character* Owner);

	UFUNCTION(BlueprintPure, Category = "Stats")
	AAIS_Character* GetOwnerCharacter() { return OwnerCharacter; };

	UFUNCTION(BlueprintCallable, Category = "Stats")
	void PopulateStats();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Stats")
	void AddStatWidget(UStat* Stat);

private:
	UPROPERTY()
	AAIS_Character* OwnerCharacter;
};
