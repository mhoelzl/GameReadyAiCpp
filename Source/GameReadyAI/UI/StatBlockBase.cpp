// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "StatBlockBase.h"
#include "StatBarBase.h"

UStatBlockBase::UStatBlockBase(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	UE_LOG(LogTemp, Log, TEXT("Creating UStatBlockBase widget"));
}

void UStatBlockBase::SetOwnerCharacter(AAIS_Character* Owner)
{
	OwnerCharacter = Owner;
	PopulateStats();
}

void UStatBlockBase::PopulateStats()
{
	check(OwnerCharacter && !OwnerCharacter->IsPendingKill() && OwnerCharacter->IsValidLowLevel());

	const TArray<UStat*>& Stats{ OwnerCharacter->GetStats() };
	for (int Index = 0; Index < Stats.Num(); ++Index)
	{
		UStat* Stat{ Stats[Index] };
		if (Stat != nullptr)
		{
			AddStatWidget(Stat);
		}
	}
}
