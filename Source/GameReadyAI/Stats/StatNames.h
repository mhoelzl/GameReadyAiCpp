#pragma once
#include "NameTypes.h"

/*******************************************************************
 * Names for various commonly used stats to avoid typos.
 *******************************************************************/
namespace StatName
{
	static const FName Charisma{ FName(TEXT("Charisma")) };
	static const FName Dexterity{ FName(TEXT("Dexterity")) };
	static const FName Happiness{ FName(TEXT("Happiness")) };
	static const FName Health{ FName(TEXT("Health")) };
	static const FName Hunger{ FName(TEXT("Hunger")) };
	static const FName Intelligence{ FName(TEXT("Intelligence")) };
	static const FName Mana{ FName(TEXT("Mana")) };
	static const FName Speed{ FName(TEXT("Speed")) };
	static const FName Speech{ FName(TEXT("Speech")) };
	static const FName Spirituality{ FName(TEXT("Spirituality")) };
	static const FName Stamina{ FName(TEXT("Stamina")) };
	static const FName Strength{ FName(TEXT("Strength")) };
}