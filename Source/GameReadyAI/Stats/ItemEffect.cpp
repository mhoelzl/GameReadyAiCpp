// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "ItemEffect.h"

float UItemEffect::EstimateEffect(const AActor* Item, const AActor* User) const
{
	checkNoEntry();
	return 0.0f;
}

float UItemEffect::ApplyEffect(const AActor* Item, AActor* User)
{
	checkNoEntry();
	return 0.0f;
}

void UItemEffect::Initialize(FName StatName, float StatDelta)
{
	checkNoEntry();
}
