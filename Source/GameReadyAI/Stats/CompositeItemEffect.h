// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Stats/ItemEffect.h"
#include "CompositeItemEffect.generated.h"

/**
 * An effect that effects a single stat in a straightforward way.
 */
UCLASS(EditInlineNew)
class GAMEREADYAI_API UCompositeItemEffect : public UItemEffect
{
	GENERATED_BODY()
	
public:
	virtual void Initialize(FName StatName, float StatDelta) override;
	
	virtual float EstimateEffect(const AActor* /* IIsUsable */ Item, const AActor* /* IItemUser */ User) const override;
	virtual float ApplyEffect(const AActor* /* IIsUsable */ Item, AActor* /* IItemUser */ User) override;

	UFUNCTION(BlueprintPure, Category = "Stats")
	TArray<UItemEffect*> GetSharedItemEffects() const { return SharedEffects; };

	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetItemEffects(TArray<UItemEffect*> NewEffects) { SharedEffects = NewEffects; };

	UFUNCTION(BlueprintCallable, Category = "Stats")
	TArray<UItemEffect*> GetEstimationOnlyEffects() const { return EstimationOnlyEffects; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetEstimationOnlyEffects(TArray<UItemEffect*> NewEffects) { EstimationOnlyEffects = NewEffects; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	TArray<UItemEffect*> GetApplicationOnlyEffects() const { return ApplicationOnlyEffects; }
	
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetApplicationOnlyEffects(TArray<UItemEffect*> NewEffects) { ApplicationOnlyEffects = NewEffects; }
protected:
	UPROPERTY(EditAnywhere, Instanced, Category = "Stats")
	TArray<UItemEffect*> SharedEffects;

	UPROPERTY(EditAnywhere, Instanced, Category = "Stats")
	TArray<UItemEffect*> EstimationOnlyEffects;

	UPROPERTY(EditAnywhere, Instanced, Category = "Stats")
	TArray<UItemEffect*> ApplicationOnlyEffects;
};
