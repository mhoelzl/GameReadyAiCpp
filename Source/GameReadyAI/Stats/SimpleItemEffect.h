// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Stats/ItemEffect.h"
#include "SimpleItemEffect.generated.h"

/**
 * An effect that effects a single stat in a straightforward way.
 */
UCLASS(EditInlineNew)
class GAMEREADYAI_API USimpleItemEffect : public UItemEffect
{
	GENERATED_BODY()
	
public:
	virtual float EstimateEffect(const AActor* Item, const AActor* User) const override;

	virtual float ApplyEffect(const AActor* Item, AActor* User) override;

	UFUNCTION(BlueprintPure, Category = "Stats")
	FName GetStatName() { return StatName; };

	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetStatName(FName NewName) { StatName = NewName; };

	UFUNCTION(BlueprintPure, Category = "Stats")
	float GetStatDelta() { return StatDelta; };

	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetStatDelta(float NewValue) { StatDelta = NewValue; };

	virtual void Initialize(FName StatName, float StatDelta);

protected:
	UPROPERTY(EditAnywhere, Category = "Stats")
	FName StatName;

	UPROPERTY(EditAnywhere, Category = "Stats")
	float StatDelta;	
};
