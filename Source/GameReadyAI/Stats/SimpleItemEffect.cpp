// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "SimpleItemEffect.h"
#include "Interfaces/IsUsable.h"
#include "Interfaces/ItemUser.h"
#include "Stats/Stat.h"



void USimpleItemEffect::Initialize(FName StatName, float StatDelta)
{
	this->StatName = StatName;
	this->StatDelta = StatDelta;
}

float USimpleItemEffect::EstimateEffect(const AActor* Item,	const AActor* UserActor) const
{
	const IItemUser* User{ Cast<IItemUser>(UserActor) };
	if (User)
	{
		TArray<UStat*> Stats{ User->GetAllStats() };
		for (UStat* Stat : Stats)
		{
			if (Stat->GetStatName() == StatName)
			{
				return Stat->DeltaUtility(StatDelta);
			}
		}
	}
	return 0.0f;
}

float USimpleItemEffect::ApplyEffect(const AActor* Item, AActor* UserActor)
{
	IItemUser* User{ Cast<IItemUser>(UserActor) };
	if (User)
	{
		return User->AddToCurrentStatValue(StatName, StatDelta);
	}
	return 0.0f;
}
