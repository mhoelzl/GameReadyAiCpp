// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Object.h"
#include "Stat.generated.h"

USTRUCT(BlueprintType)
struct FStatTableRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	// The initial value for this stat.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
	float InitialValue;

	// The minimal value to which this stat is clamped.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float MinValue;

	// The maximal value to which this stat is clamped.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float MaxValue;

	// The name of the importance curve for this stat.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	TAssetPtr<UCurveFloat> UtilityCurve;

	// A constant importance value for cases where more detailed control is not needed.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float ImportanceFactor;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStatChange);

/**
 * A single stat.
 */
UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class GAMEREADYAI_API UStat : public UObject
{
	GENERATED_BODY()

public:
	// Needed since the Unreal Header Tool does not recognize the following constructor with 
	// all arguments defaulted as default constructor.
	UStat(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	void Initialize(UObject* Outer, FName Name, float CurrentValue = 100.0f, float MinValue = 0.0f,
		float MaxValue = 100.0f, float ImportanceFactor = 1.0f, const UCurveFloat* const ImportanceCurve = nullptr);
	void Initialize(UObject* Outer, FName Name, const FStatTableRow* const TableRow);

	UPROPERTY(BlueprintAssignable)
	FStatChange OnStatChanged;

	// The name of this stat.
	UFUNCTION(BlueprintPure, Category = "Stats")
	FName GetStatName() { return StatName; }

	// The current value of the stat for the actor owning this instance.
	UFUNCTION(BlueprintPure, Category = "Stats")
	float GetCurrentValue() { return CurrentValue; }
	
	// The current percentage between min and max of the stat for the actor owning this instance.
	UFUNCTION(BlueprintPure, Category = "Stats")
	float GetCurrentPercentage() { return (CurrentValue - MinValue) / (MaxValue - MinValue); }

	// Set the current value of the stat for the actor owning this instance.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetCurrentValue(float NewValue);

	// Add Delta to the current value of the stat for the actor owning this instance.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	float AddToCurrentValue(float Delta);

	// The minimal value of the stat for the actor owning this instance.
	UFUNCTION(BlueprintPure, Category = "Stats")
	float GetMinValue() { return MinValue; }
	
	// Set the minimal value of the stat for the actor owning this instance.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetMinValue(float NewValue);

	// The maximal value of the stat for the actor owning this instance.
	UFUNCTION(BlueprintPure, Category = "Stats")
	float GetMaxValue() { return MaxValue; }

	// Set the maximal value of the stat for the actor owning this instance.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetMaxValue(float NewValue);

	// The importance factor of the stat for the actor owning this instance.
	UFUNCTION(BlueprintPure, Category = "Stats")
	float GetImportanceFactor() { return ImportanceFactor; }

	// Set the importance factor of the stat for the actor owning this instance.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetImportanceFactor(float NewValue);

	// The desired value of the stat for the actor owning this instance.
	// Note that this evaluates the utility only at curve points. If you use curves with
	// polynomial interpolation this function will not find the true desired value.
	UFUNCTION(BlueprintPure, Category = "Stats")
	float GetDesiredValue();

	// The utility of this stat when it has the given Value, i.e., how good Value is for the agent.
	UFUNCTION(BlueprintPure, Category = "Stats")
	float Utility(float Value);
	float Utility() { return Utility(CurrentValue); }

	// The change in utility that changing the stat by Delta would have.
	UFUNCTION(BlueprintPure, Category = "Stats")
	float DeltaUtility(float Delta);

protected:
	UPROPERTY(BlueprintAssignable)
	FStatChange StatChangedEvent;

	// The name of this stat.
	UPROPERTY(EditAnywhere, Category = "Stats")
	FName StatName;

	// The initial value for this stat.
	UPROPERTY(EditAnywhere, Category = "Stats")
	float CurrentValue;

	// The minimal value to which this stat is clamped.
	UPROPERTY(EditAnywhere, Category = "Stats")
	float MinValue;

	// The maximal value to which this stat is clamped.
	UPROPERTY(EditAnywhere, Category = "Stats")
	float MaxValue;

	// A constant importance factor for cases where more detailed control is not needed.
	// If ImportanceCurve is a nullptr, the importance is computed as
	//      ImportanceFactor * Abs(DesiredValue - CurrentValue) / (MaxValue - MinValue)
	UPROPERTY(EditAnywhere, Category = "Stats")
	float ImportanceFactor;

	// A curve that describes the utility of possible values.
	UPROPERTY(EditAnywhere, Category = "Stats")
	const UCurveFloat* UtilityCurve;
};
