// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "ItemEffect.h"
#include "Stat.h"


UStat::UStat(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
}

void UStat::Initialize(UObject* Outer, FName Name, float CurrentValue /*= 100.0f*/, float MinValue /*= 0.0f*/, float MaxValue /*= 100.0f*/,
	float ImportanceFactor /*= 1.0f*/, const UCurveFloat* const UtilityCurve /*= nullptr */)
{
	this->StatName = Name;
	this->CurrentValue = CurrentValue;
	this->MinValue = MinValue;
	this->MaxValue = MaxValue;
	this->ImportanceFactor = ImportanceFactor;
	this->UtilityCurve = UtilityCurve;

	ensure(MinValue <= MaxValue);
	ensure(MinValue <= CurrentValue);
	ensure(CurrentValue <= MaxValue);
}

void UStat::Initialize(UObject* Outer, FName Name, const FStatTableRow* const TableRow)
{
	Initialize(Outer, Name, TableRow->InitialValue, TableRow->MinValue, TableRow->MaxValue,
		TableRow->ImportanceFactor, TableRow->UtilityCurve.Get());
}

void UStat::SetCurrentValue(float NewValue)
{
	CurrentValue = FMath::Clamp(NewValue, MinValue, MaxValue);
	OnStatChanged.Broadcast();
}

float UStat::AddToCurrentValue(float Delta)
{
	float Result = CurrentValue + Delta;
	SetCurrentValue(Result);
	return Result;
}

void UStat::SetMinValue(float NewValue)
{
	MinValue = NewValue;
	OnStatChanged.Broadcast();
}

void UStat::SetMaxValue(float NewValue)
{
	MaxValue = NewValue;
	OnStatChanged.Broadcast();
}

void UStat::SetImportanceFactor(float NewValue)
{
	ImportanceFactor = NewValue;
	OnStatChanged.Broadcast();
}

float UStat::GetDesiredValue()
{
	if (ensure(UtilityCurve))
	{
		const FRichCurve& Curve = UtilityCurve->FloatCurve;
		const TArray<FRichCurveKey>& Keys = Curve.Keys;

		if (Keys.Num() == 0)
		{
			return 0.0f;
		}

		float CurrentValue{ Keys[0].Time };
		float UtilityOfCurrentValue{ Utility(CurrentValue) };
		for (int i = 1; i < Keys.Num(); ++i)
		{
			float NewValue{ Keys[i].Time };
			float NewUtility{ Utility(NewValue) };
			if (NewUtility > UtilityOfCurrentValue)
			{
				CurrentValue = NewValue;
			}
		}
		return CurrentValue;
	}
	else
	{
		return MaxValue;
	}
}

float UStat::Utility(float Value)
{
	if (ensure(UtilityCurve))
	{
		return ImportanceFactor * UtilityCurve->GetFloatValue(Value);
	}
	else
	{
		return ImportanceFactor * FMath::Abs(MaxValue - Value) / (MaxValue - MinValue);
	}
}

float UStat::DeltaUtility(float Delta)
{
	float ValueAfterEffect{ FMath::Clamp(CurrentValue + Delta, MinValue, MaxValue) };
	return Utility(ValueAfterEffect) - Utility();
}
