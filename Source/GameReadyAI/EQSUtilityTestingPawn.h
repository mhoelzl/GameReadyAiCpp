// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "EnvironmentQuery/EQSTestingPawn.h"
#include "Interfaces/ItemUser.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Stats/Stat.h"
#include "EQSUtilityTestingPawn.generated.h"

/**
 * A testing pawn that contains a perception component and stats.
 */
UCLASS()
class GAMEREADYAI_API AEQSUtilityTestingPawn : public AEQSTestingPawn, public IItemUser
{
	GENERATED_BODY()

public:
	AEQSUtilityTestingPawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	// We may need to initialize our perception component before running the query.
	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;

	virtual void PostLoad() override;
	virtual void PostActorCreated() override;
	virtual void BeginPlay() override;

	void InitializeStats();
	void InitializePerceptionComponent();

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	// Perception system
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Perception")
	UAIPerceptionComponent* PerceptionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Perception")
	UAISenseConfig_Sight* SightConfig;

	// IItemUser interface
	virtual float EstimateUtilityOf(const AActor* ItemToUse) const override;

	virtual bool UseItemInWorld(AActor* ItemToUse) override;

	virtual float AddToCurrentStatValue(FName StatName, float Delta) override;

	virtual TArray<UStat*> GetAllStats(void) const override;
	// End IItemUser interface 

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	UStat* GetStatOrNullptr(FName StatName) { return StatMap.FindRef(StatName); };

	UFUNCTION(BlueprintPure, Category = "Player Stats")
	TArray<UStat*> GetStats() const { return Stats; };

	UPROPERTY(EditAnywhere, Instanced, Category = "Player Stats", Meta = (AllowPrivateAccess = "true"))
	TArray<UStat*> Stats;

	UPROPERTY()
	TMap<FName, UStat*> StatMap;
	
	UPROPERTY(EditAnywhere, Category = "Player Stats")
	bool bAreStatsInitialized;
};
