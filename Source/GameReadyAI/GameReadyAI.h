// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#ifndef __GAMEREADYAI_H__
#define __GAMEREADYAI_H__

#include "Engine.h"

#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

#endif
