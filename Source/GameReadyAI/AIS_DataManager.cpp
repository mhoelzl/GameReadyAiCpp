// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AIS_GameEngine.h"
#include "AIS_DataManager.h"
#include "AIS_DataManagerProvider.h"

UAIS_DataManager::UAIS_DataManager(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	static auto StatTableFinder{ ConstructorHelpers::FObjectFinder<UDataTable>(TEXT("DataTable'/Game/Data/Stats.Stats'")) };
	if (!StatTable && StatTableFinder.Succeeded())
	{
		StatTable = StatTableFinder.Object;
	}
	check(StatTable != nullptr && StatTable->IsValidLowLevel());
}

UAIS_DataManager* UAIS_DataManager::Get()
{
	UEngine* Engine{ GEngine };
	if (ensure(Engine != nullptr))
	{
		IAIS_DataManagerProvider* DataManagerProvider{ Cast<IAIS_DataManagerProvider>(Engine) };
		if (ensure(DataManagerProvider != nullptr))
		{
			UAIS_DataManager* DataManager{ DataManagerProvider->GetDataManager() };
			check(DataManager != nullptr && DataManager->IsValidLowLevel());
			return DataManager;
		}
	}

	return nullptr;
}
