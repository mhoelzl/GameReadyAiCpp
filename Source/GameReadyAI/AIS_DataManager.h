// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Object.h"
#include "Engine/StreamableManager.h"
#include "AIS_DataManager.generated.h"

/**
 * Manager for all persistent data, e.g., tables that are loaded from CSV files.
 */
UCLASS()
class GAMEREADYAI_API UAIS_DataManager : public UObject
{
	GENERATED_BODY()

public:
	UAIS_DataManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	static UAIS_DataManager* Get();

	class UDataTable* GetStatTable() { return StatTable; };
	FStreamableManager& GetStreamableManager() { return StreamableManager; };

protected:

	// Persistent table to initialize character stats and item effects.
	UPROPERTY()
	class UDataTable* StatTable;

	// Manage the loading of assets, e.g., to resolve TAssetPtr<>s.
	FStreamableManager StreamableManager;

};
