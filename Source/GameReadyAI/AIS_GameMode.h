// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "GameFramework/GameMode.h"
#include "AIS_GameMode.generated.h"

UCLASS(minimalapi)
class AAIS_GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AAIS_GameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

};



