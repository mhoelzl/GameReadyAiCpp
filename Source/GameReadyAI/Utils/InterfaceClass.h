// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Runtime/CoreUObject/Public/UObject/UnrealType.h"
#include "InterfaceClass.generated.h"

/**
 * A wrapper for interface types to provide details panel customization.
 */
USTRUCT()
struct GAMEREADYAI_API FInterfaceClass
{
	GENERATED_USTRUCT_BODY()

	// The value set by the user
	UPROPERTY(EditAnywhere)
	UClass* Class;

	virtual const UClass* GetUClass() const { return Class; };

	// Transparently convert into a UClass* to do justice to the struct name...
	operator const UClass*() { return GetUClass(); };

	bool operator== (const FInterfaceClass& OtherInterface)
	{
		return GetUClass() == OtherInterface.GetUClass();
	}

	bool operator== (const UClass* OtherUClass)
	{
		return GetUClass() == OtherUClass;
	}
};