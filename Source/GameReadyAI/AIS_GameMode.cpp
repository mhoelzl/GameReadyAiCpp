// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AIS_PlayerPawn.h"
#include "AIS_PlayerController.h"
#include "Engine/DataTable.h"
#include "AIS_GameMode.h"

AAIS_GameMode::AAIS_GameMode(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get() */) :
	Super(ObjectInitializer)
{
	DefaultPawnClass = AAIS_PlayerPawn::StaticClass();
	PlayerControllerClass = AAIS_PlayerController::StaticClass();
}
