// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GameReadyAI : ModuleRules
{
	public GameReadyAI(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine",
            "InputCore", "UMG" });
        PrivateDependencyModuleNames.AddRange(new string[] { "SlateCore", "Slate" });
	}
}
