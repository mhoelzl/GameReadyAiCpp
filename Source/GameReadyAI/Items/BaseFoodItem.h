// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "GameFramework/Actor.h"
#include "GenericTeamAgentInterface.h"
#include "Interfaces/IsUsable.h"
#include "Interfaces/ItemUser.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISightTargetInterface.h"
#include "Stats/ItemEffect.h"
#include "BaseFoodItem.generated.h"

/**
 * Superclass for all food items.
 */
UCLASS()
class GAMEREADYAI_API ABaseFoodItem : 
	public AActor,
	// Can be used by AIS_Characters (or, more generally, item users).
	public IIsUsable,
	// Modify visibility so that traces go to the center of mass, not the actor location.
	public IAISightTargetInterface, 
	// We don't currently make use of the team interface.
	public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	ABaseFoodItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void PostInitializeComponents() override;

	UFUNCTION(BlueprintPure, Category = "Item")
	virtual FVector ItemLocation() const override;

	const TArray<UItemEffect*> GetSharedEffects() const override { return ItemEffects; };

	virtual bool CanBeSeenFrom(const FVector& ObserverLocation, FVector& OutSeenLocation,
		int32& NumberOfLoSChecksPerformed, float& OutSightStrength, const AActor* IgnoreActor = NULL) const override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Static Mesh", Meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Perception", Meta = (AllowPrivateAccess = "true"))
	UAIPerceptionStimuliSourceComponent* StimulousSource;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Instanced, Category = "Effects", Meta = (AllowPrivateAccess = "true"))
	TArray<UItemEffect*> ItemEffects;

	// Implementation of the IsUsable interface
	virtual float EstimateUtilityFor(const AActor* User) const override;

	virtual bool ApplyItemEffectsTo(AActor* User) override;
};
