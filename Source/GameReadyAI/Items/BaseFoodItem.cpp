// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AI/AIS_Character.h"
#include "BaseFoodItem.h"
#include "Perception/AISense_Sight.h"
#include "Stats/StatNames.h"
#include "Stats/SimpleItemEffect.h"

ABaseFoodItem::ABaseFoodItem(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	USimpleItemEffect* ItemEffect{ CreateDefaultSubobject<USimpleItemEffect>("ItemEffect") };
	ItemEffect->Initialize(StatName::Stamina, 50.0f);
	ItemEffects.Add(ItemEffect);

	StimulousSource = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>("StimulousSource");

	PrimaryActorTick.bCanEverTick = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	if (RootComponent)
	{
		StaticMesh->AttachTo(RootComponent);
	}
	else
	{
		SetRootComponent(StaticMesh);
	}
}

void ABaseFoodItem::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	UE_LOG(LogTemp, Log, TEXT("Registering %s with perception system: PIC"), *GetNameSafe(this));
	StimulousSource->RegisterForSense(UAISense_Sight::StaticClass());
}

FVector ABaseFoodItem::ItemLocation() const
{
	UPrimitiveComponent* BodyComponent{ Cast<UPrimitiveComponent>(RootComponent) };
	if (BodyComponent)
	{
		return BodyComponent->GetCenterOfMass();
	}
	return GetActorLocation();
}

// Do a line trace to the center of mass, otherwise things like barrels are not perceived.
bool ABaseFoodItem::CanBeSeenFrom(const FVector& ObserverLocation, FVector& OutSeenLocation, int32& NumberOfLoSChecksPerformed, float& OutSightStrength, const AActor* IgnoreActor /*= NULL*/) const
{
	NumberOfLoSChecksPerformed = 1;
	OutSightStrength = 0.0f;

	UPrimitiveComponent* BodyComponent{ Cast<UPrimitiveComponent>(RootComponent) };
	if (BodyComponent != nullptr)
	{
		FVector TargetLocation{ BodyComponent->GetCenterOfMass() };
		FHitResult HitResult;
		const FCollisionQueryParams Params;
		bool Result{ BodyComponent->LineTraceComponent(HitResult, ObserverLocation, TargetLocation, Params) };
		if (Result)
		{
			OutSightStrength = 1.0f;
			OutSeenLocation = TargetLocation;
			return true;
		}
	}

	// This is not exactly true, but it works as long as a mesh or collision component
	// is the root component of the food item.
	return false;
}

float ABaseFoodItem::EstimateUtilityFor(const AActor* User) const
{
	float Result{ 0.0f };
	for (UItemEffect* Effect : ItemEffects)
	{
		Result += Effect->EstimateEffect(this, User);
	}
	return Result;
}

bool ABaseFoodItem::ApplyItemEffectsTo(AActor* User)
{
	FString UserName{ User ? User->GetName() : FString(TEXT("unnamed object")) };
	UE_LOG(LogTemp, Log, TEXT("*** %s used %s ***"), *UserName, *GetClass()->GetName());

	for (UItemEffect* Effect : ItemEffects)
	{
		Effect->ApplyEffect(this, User);
	}
	// StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Destroy();
	return true;
}
