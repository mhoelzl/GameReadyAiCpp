// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Items/BaseFoodItem.h"
#include "BarrelFood.generated.h"

/**
 * 
 */
UCLASS()
class GAMEREADYAI_API ABarrelFood : public ABaseFoodItem
{
	GENERATED_BODY()
	
public:
	ABarrelFood(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
};
