// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "PlantFoodItem.h"
#include "Stats/SimpleItemEffect.h"
#include "Stats/StatNames.h"

APlantFoodItem::APlantFoodItem(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	check(StaticMesh);
	static auto ApplePath = TEXT("StaticMesh'/Game/AIStream/Meshes/Apple.Apple'");
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshFinder(ApplePath);
	if (MeshFinder.Succeeded())
	{
		StaticMesh->SetStaticMesh(MeshFinder.Object);
	}

	UItemEffect* HealthEffect{ CreateDefaultSubobject<USimpleItemEffect>("HealthEffect") };
	HealthEffect->Initialize(StatName::Health, 10);
	ItemEffects.Add(HealthEffect);

	UItemEffect* HappinessEffect{ CreateDefaultSubobject<USimpleItemEffect>("HappinessEffect") };
	HappinessEffect->Initialize(StatName::Happiness, -10);
	ItemEffects.Add(HappinessEffect);
}
