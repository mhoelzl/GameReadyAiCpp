// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Items/BaseFoodItem.h"
#include "BadFoodItem.generated.h"

/**
 * A food item that kills the player.
 */
UCLASS()
class GAMEREADYAI_API ABadFoodItem : public ABaseFoodItem
{
	GENERATED_BODY()
	
public:
	ABadFoodItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
};
