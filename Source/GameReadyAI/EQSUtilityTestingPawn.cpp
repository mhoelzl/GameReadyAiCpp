// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AIS_DataManager.h"
#include "Interfaces/IsUsable.h"
#include "EQSUtilityTestingPawn.h"


AEQSUtilityTestingPawn::AEQSUtilityTestingPawn(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/)
	: Super(ObjectInitializer)
{
	PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));
	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfig"));
}

void AEQSUtilityTestingPawn::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{

	Super::TickActor(DeltaTime, TickType, ThisTickFunction);
}

void AEQSUtilityTestingPawn::PostLoad()
{
	Super::PostLoad();
	InitializeStats();
	InitializePerceptionComponent();
}

void AEQSUtilityTestingPawn::PostActorCreated()
{
	Super::PostActorCreated();
	InitializeStats();
	InitializePerceptionComponent();
}


void AEQSUtilityTestingPawn::BeginPlay()
{
	if (PerceptionComponent->GetSenseConfig(SightConfig->GetSenseID()) == nullptr)
	{
		InitializePerceptionComponent();
	}
}

#if WITH_EDITOR
void AEQSUtilityTestingPawn::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	UProperty* Property = PropertyChangedEvent.Property;
	if (!Property)
	{
		return;
	}
	UE_LOG(LogTemp, Log, TEXT("Changed character property %s of %s."), *Property->GetName(), *GetName());
	if (Property->GetFName() == GET_MEMBER_NAME_CHECKED(AEQSUtilityTestingPawn, Stats))
	{
		UE_LOG(LogTemp, Log, TEXT("Updating StatMap for character."));
		StatMap.Empty();
		for (UStat* Stat : Stats)
		{
			if (Stat != nullptr)
			{
				StatMap.Add(Stat->GetStatName(), Stat);
			}
		}
	}
}
#endif


float AEQSUtilityTestingPawn::EstimateUtilityOf(const AActor* ItemToUse) const
{
	const IIsUsable* Item{ Cast<IIsUsable>(ItemToUse) };
	if (Item)
	{
		return Item->EstimateUtilityFor(this);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("*** Item %s does not implement interface IsUsable ***"), *ItemToUse->GetName());
	}
	return 0.0f;
}

bool AEQSUtilityTestingPawn::UseItemInWorld(AActor* ItemToUse)
{
	IIsUsable* Item{ Cast<IIsUsable>(ItemToUse) };
	if (Item)
	{
		return Item->ApplyItemEffectsTo(this);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("*** Item %s does not implement interface IsUsable ***"), *ItemToUse->GetName());
	}
	return false;
}

float AEQSUtilityTestingPawn::AddToCurrentStatValue(FName StatName, float Delta)
{

	UStat* Stat = GetStatOrNullptr(StatName);
	if (Stat == nullptr)
	{
		UE_LOG(LogTemp, Log, TEXT("Adding to non-existent stat value %s of actor %s."), *StatName.ToString(), *GetNameSafe(this));
		return 0.0f;
	}
	else
	{
		return Stat->AddToCurrentValue(Delta);
	}
}

TArray<UStat*> AEQSUtilityTestingPawn::GetAllStats(void) const
{
	return Stats;
}


void AEQSUtilityTestingPawn::InitializeStats()
{
	if (!GEngine)
	{
		return;
	}
	UAIS_DataManager* DataManager{ UAIS_DataManager::Get() };
	if (!ensure(DataManager))
	{
		return;
	}
	if (bAreStatsInitialized && StatMap.Num() > 0)
	{
		return;
	}
	UDataTable* StatTable{ DataManager->GetStatTable() };
	if (!ensure(StatTable != nullptr && StatTable->IsValidLowLevel()))
	{
		return;
	}
	FStreamableManager& StreamableManager{ DataManager->GetStreamableManager() };

	Stats.Empty();
	StatMap.Empty();

	TArray<FName> StatNames{ StatTable->GetRowNames() };
	for (FName StatName : StatNames)
	{
		FStatTableRow* StatData{ StatTable->FindRow<FStatTableRow>(StatName, FString(TEXT("GameMode::StatTable"))) };
		UCurveFloat* UtilityCurve = StatData->UtilityCurve.Get();
		if (!UtilityCurve)
		{
			UtilityCurve = Cast<UCurveFloat>(StreamableManager.SynchronousLoad(StatData->UtilityCurve.ToStringReference()));
		}
		ensure(UtilityCurve);

		UStat* Stat{ NewObject<UStat>(this) };
		Stat->Initialize(this, StatName, StatData);
		int32 Index{ Stats.Add(Stat) };
		StatMap.Add(StatName, Stat);

		// We set this inside the loop so that we don't set stats to initialized if we happen to receive an empty 
		// stat table.
		bAreStatsInitialized = true;
	}
}

void AEQSUtilityTestingPawn::InitializePerceptionComponent()
{
	SightConfig->SightRadius = 3000.0f;
	SightConfig->LoseSightRadius = 3500.0f;
	SightConfig->PeripheralVisionAngleDegrees = 90.0f;
	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	UFloatProperty* MaxAgeProperty{
		Cast<UFloatProperty>(UAISenseConfig::StaticClass()->FindPropertyByName(FName(TEXT("MaxAge")))) };
	if (ensure(MaxAgeProperty))
	{
		uint8* ConfigPtr{ MaxAgeProperty->ContainerPtrToValuePtr<uint8>(SightConfig) };
		MaxAgeProperty->SetFloatingPointPropertyValue(ConfigPtr, 10.0f);
	}

	UAIPerceptionSystem* PerceptionSystem{ UAIPerceptionSystem::GetCurrent(GetWorld()) };
	if (PerceptionSystem)
	{
		if (!PerceptionComponent->IsRegistered())
		{
			PerceptionComponent->RegisterComponent();
		}
		PerceptionComponent->ConfigureSense(*SightConfig);
		PerceptionComponent->SetDominantSense(SightConfig->GetSenseImplementation());
	}
}
