#pragma once

#include "AIS_DataManager.h"
#include "AIS_DataManagerProvider.generated.h"

/*
 * Interface implemented by all classes that provide a data manager (i.e., all three engine classes).
 * This is necessary so that we can implement the static DataManager::Get() function without
 * knowing which engine class we will obtain.
 */
UINTERFACE(NotBlueprintable, Meta = (CannotImplementInterfaceInBlueprint = "true"))
class GAMEREADYAI_API UAIS_DataManagerProvider : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class GAMEREADYAI_API IAIS_DataManagerProvider
{
	GENERATED_IINTERFACE_BODY()

	UFUNCTION()
	virtual UAIS_DataManager* GetDataManager() = 0;
};
