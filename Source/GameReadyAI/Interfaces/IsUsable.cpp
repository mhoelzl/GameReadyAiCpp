// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "IsUsable.h"
#include "Stats/ItemEffect.h"

UIsUsable::UIsUsable(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
}

float IIsUsable::EstimateUtilityFor(const AActor* /* IItemUser */ User) const
{
	float Result{ 0.0f };

	const AActor* ThisAsActor{ Cast<AActor>(this) };
	for (const UItemEffect* Effect : GetSharedEffects())
	{
		Result += Effect->EstimateEffect(ThisAsActor, User);
	}

	for (const UItemEffect* Effect : GetEstimationOnlyEffects())
	{
		Result += Effect->EstimateEffect(ThisAsActor, User);
	}
	return Result;
}

bool IIsUsable::ApplyItemEffectsTo(AActor* /* IItemUser */ User)
{
	const AActor* ThisAsActor{ Cast<AActor>(this) };
	for (UItemEffect* Effect : GetSharedEffects())
	{
		Effect->ApplyEffect(ThisAsActor, User);
	}

	for (UItemEffect* Effect : GetApplicationOnlyEffects())
	{
		Effect->ApplyEffect(ThisAsActor, User);
	}

	return true;
}

const TArray<UItemEffect*> IIsUsable::GetEstimationOnlyEffects() const
{

	static const TArray<UItemEffect*> NoEffects{};
	return NoEffects;
}

const TArray<UItemEffect*> IIsUsable::GetApplicationOnlyEffects() const
{

	static const TArray<UItemEffect*> NoEffects{};
	return NoEffects;
}
