// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "ItemUser.generated.h"

/**
 * An interface for actors that can use usable items. 
 */
UINTERFACE(Blueprintable, Meta = (CannotImplementInterfaceInBlueprint = "true"))
class UItemUser : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class IItemUser
{
	GENERATED_IINTERFACE_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Player Stats")
	virtual float EstimateUtilityOf(const AActor* Item) const = 0;

	UFUNCTION(BlueprintCallable, Category = "Player Stats")
	virtual bool UseItemInWorld(AActor* Item) = 0;

	UFUNCTION(BlueprintCallable, Category = "Player Stats")
	virtual float AddToCurrentStatValue(const FName StatName, const float Delta) = 0;

	UFUNCTION(BlueprintCallable, Category = "Player Stats")
	virtual TArray<UStat*> GetAllStats() const = 0;
};