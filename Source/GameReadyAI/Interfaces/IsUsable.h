// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once
#include "IsUsable.generated.h"

/**
 * Interface for usable items
 */
UINTERFACE(Blueprintable, BlueprintType, Meta = (CannotImplementInterfaceInBlueprint))
class GAMEREADYAI_API UIsUsable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class GAMEREADYAI_API IIsUsable
{
	GENERATED_IINTERFACE_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Item")
	virtual FVector ItemLocation() const = 0;

	UFUNCTION(BlueprintCallable, Category = "Item")
	virtual float EstimateUtilityFor(const AActor* /* IItemUser */ User) const;

	UFUNCTION(BlueprintCallable, Category = "Item")
	virtual bool ApplyItemEffectsTo(AActor* /* IItemUser */ User);
	
	UFUNCTION(BlueprintCallable, Category = "Stats")
	virtual const TArray<UItemEffect*> GetSharedEffects() const = 0;

	UFUNCTION(BlueprintCallable, Category = "Stats")
	virtual const TArray<UItemEffect*> GetEstimationOnlyEffects() const;

	UFUNCTION(BlueprintCallable, Category = "Stats")
	virtual const TArray<UItemEffect*> GetApplicationOnlyEffects() const;
};
