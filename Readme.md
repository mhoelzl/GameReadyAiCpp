Game-Ready AI in C++
====================

This repository contains a C++ implementation of a god-game AI that is
loosely based on the
["Making Game Ready AI Tutorial"](https://www.youtube.com/watch?v=PgxuaTSkyu4)
from Epic Games.

My original idea was to closely follow the videos in the stream but to
implement most things in C++ instead of Blueprints. However many
design decisions that work well in Blueprints don't transfer readily
to a more static language, so I felt that I was fighting the compiler
most of the time.  Therefore I decided to follow the topics presented
in the stream, but to build the AI in a manner that plays more closely
to C++'s strengths and avoids many of the weaknesses of a literal
translation. Or so I hope.

The result is the code in this repository.  Currently the state of the
repository reflects the first four videos of the live stream, i.e,
characters are controlled by EQS and Behavior Trees, but they make no
use of sensing. Characters have stats that influence their behavior
and these stats are highly configurable.  Some features that are not
discussed in the live stream but might be of interest are:

* The stat system is based on data tables so you can easily modify the
  data in an Excel sheet without touching any code.

* To make data tables work the project is based on a custom game
  engine class that manages persistent data. (This does *not* mean
  that you have to compile the engine from source!)
  
* The code contains custom Behavior Tree tasks and services as well as
  custom EQS generator and test classes implemented in C++. The EQS
  generator is rather general and allows you to find all actors
  implementing a (user selectable) interface.

* To allow the selection of the interface type the EQS generator uses
  a custom property type layout.

This repository uses git-lfs to deal with the content, so you need to
install git-lfs before being able to check it out.  I build against
the GitHub master branch, but I occasionally test with 4.11, so the
code should probably be easy to compile using the latest pre-release
from the launcher. I use some helper functions for Behavior Trees that
are not present in 4.10, so you may need to make a few minor updates
if you want to build against that version.

If you have any questions, suggestions or other feedback you can reach
me at <tc@xantira.com> or on Twitter at @MatthiasHoelzl.

Some Differences from the Live-Stream Version
---------------------------------------------

*The following paragraphs are notes I took while still trying to
follow mostly one to one.  I'm leaving them here since they might
still be helpful for somebody looking at the code after following the
stream.  They do not represent the currents state of the project; it
contains many more differences from Ian's implementation than
described here.*

Overall I'm trying to stay relatively closely to the project as it is
developed on the stream. The most significant differences that I'm
aware of are

* The functionality of the AIS_PlayerController is implemented in a
  slightly different manner due to the differences between C++ and
  Blueprints in handling asynchronous function calls.

* I follow the naming convention of the engine and prefix behavior
  tree nodes with `BTTask_`, `BTService_`, etc.

* I've generally replaced abbreviations in variable and function names
  with their fully spelled-out counterparts. Therefore the C++
  implementation of the `Service_StamCheck` service is called
  `BTService_StaminaCheck`.

* I've changed a few variable names that I think are misleading. For
  example, `StamTickRate` is called `StaminaTickTime` since rate would
  seem to imply the number of ticks per unit of time (as in "bit
  rate"), not the time that you wait for the next tick. In other words
  I would expect the stamina decay to tick faster given a higher tick
  rate, not slower.

* I'm using different interfaces for the `UseItem` and `AIUseItem`
  functinalities, since putting both of them into the same class seems
  like pretty bad design for a statically typed language like
  C++. Therefore I have `IsUsable` for things that can be used and
  `ItemUser` for things that can use `IsUsable`-instances. I've also
  renamed the `AIUseItem` method to `UseItemInWorld` since it takes
  over that functionality.

* The line trace and distance check for using an item are performed in
  the `AIS_Character` class, not in the task, as these checks should
  be performed whenever the character tries to use an item, not only
  in the case it is triggered by this specific task.

* Instead of using parallel arrays for stats I use a single array
  containing a structure for each stat.


